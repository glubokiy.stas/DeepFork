import glob
import os
from collections import defaultdict
from typing import Dict, List, Tuple

import pandas as pd
from Levenshtein import ratio

from bets import constants as c

FORK_PAIRS = [
    (c.HANDICAP1, c.HANDICAP2),
    (c.OVER, c.UNDER),
    (c.W1, c.W2X),
    (c.W1W2, c.X),
    (c.W1X, c.W2),
]


def main():
    df1 = get_df('bet188')
    df2 = get_df('zenit')

    groups = get_groups(df1, df2)

    for k, g in groups.items():
        book1 = g[0]
        book2 = g[1]

        matches1 = get_matches(book1)
        matches2 = get_matches(book2)

        mapping = get_match_mapping(matches1, matches2)

        for match1_name, match2_name in mapping.items():
            match1 = book1[book1.match == match1_name]
            match2 = book2[book2.match == match2_name]

            for _, row in match1.iterrows():
                for _bet1, _bet2 in FORK_PAIRS:
                    for bet1, bet2 in [(_bet1, _bet2), (_bet2, _bet1)]:
                        if row['bet'] == bet1:
                            cond = (match2.bet == bet2) & (match2.bet_details == row['bet_details'])
                            if not pd.isnull(row['value']):
                                cond = cond & (match2.value == -row['value'])

                            row2 = get_row(match2,  cond)
                            if row2 is not None:
                                prob = 1 / row['coef'] + 1 / row2['coef']
                                if prob < 1:
                                    print(f'\nFork value: {1 - prob}')
                                    print(pd.DataFrame([row, row2]))


def get_df(name) -> pd.DataFrame:
    list_of_files = glob.glob(os.path.join(os.path.dirname(__file__), 'bets', 'local_output', name, '*.csv'))
    latest_file = max(list_of_files, key=os.path.getctime)
    df = pd.read_csv(latest_file).drop_duplicates()
    df['match'] = df['p1'] + ' - ' + df['p2']
    df['bet_details'] = df['part'].map(str) + df['part_number'].map(str) + df['extra'].map(str)
    return df


def get_row(df, condition):
    sliced = df[condition]
    if sliced.shape[0] == 0:
        return None
    return sliced.iloc[0]


def get_matches(df):
    return df['match'].unique()


def get_groups(*dfs) -> Dict[Tuple, List[pd.DataFrame]]:
    groups = defaultdict(list)

    for df in dfs:
        gb = df.groupby(['sport', 'datetime'])
        for x in gb.groups:
            groups[x].append(gb.get_group(x))

    for k in list(groups.keys()):
        if len(groups[k]) < 2:
            del groups[k]

    return groups


def get_match_mapping(matches1, matches2):
    mapping = {}

    for m1 in matches1:
        best_match = None
        best_ratio = None
        for m2 in matches2:
            r = ratio(m1, m2)

            if r > 0.6 and (best_ratio is None or r > best_ratio):
                best_match = m2
                best_ratio = r

        if best_match is not None:
            mapping[m1] = best_match

    return mapping


if __name__ == '__main__':
    main()
