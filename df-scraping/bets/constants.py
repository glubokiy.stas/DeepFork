# ==== BETS ====

W1 = 'win1'
W2 = 'win2'
X = 'draw'
W1X = 'win1draw'
W2X = 'win2draw'
W1W2 = 'win1win2'

HANDICAP1 = 'handicap1'
HANDICAP2 = 'handicap2'

OVER = 'over'
UNDER = 'under'

BOTH_SCORE = 'both_score'
NOT_BOTH_SCORE = 'one_not_score'


# ==== PARTS ====

QUARTER = 'quarter'
PERIOD = 'period'
HALF = 'half'
SET = 'set'
WITH_OVERTIME = 'with_overtime'
WITHOUT_OVERTIME = 'without_overtime'


# ==== SPORTS ====

BASKETBALL = 'basketball'
SOCCER = 'soccer'
ICE_HOCKEY = 'ice_hockey'
VOLLEYBALL = 'volleyball'
TENNIS = 'tennis'

SPORTS_DEF_WITH_OT = [BASKETBALL]

# ==== EXTRAS ====

BY_SETS = 'by_sets'
BY_YELLOW_CARDS = 'by_yellow_cards'
BY_OFFSIDES = 'by_offsides'
BY_CORNERS = 'by_corners'
BY_SHOTS_ON_GOAL = 'by_shots_on_goal'
BY_SHOTS = 'by_shots'
BY_FOULS = 'by_fouls'
BY_BALL_POSS = 'by_ball_poss'
