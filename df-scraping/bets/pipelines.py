from bets import constants as c
from bets.items import BetItem


class CleanPipeline(object):

    def process_item(self, item, spider):
        if isinstance(item, BetItem):
            item['bookmaker'] = spider.bookmaker
            item['lang'] = spider.lang

            for field in item.fields:
                clean_method = getattr(spider, f'clean_{field}', None)
                if clean_method is not None:
                    item[field] = clean_method(item[field])

                item.setdefault(field, None)

            if item['part'] is None:
                if item['sport'] in c.SPORTS_DEF_WITH_OT:
                    item['part'] = c.WITH_OVERTIME
                else:
                    item['part'] = c.WITHOUT_OVERTIME

        return item
