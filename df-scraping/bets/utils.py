import re


def join_selected(elem, sel):
    return ''.join(x.strip() for x in elem.css(sel).getall())


def get_row_text(elem, tag='td'):
    return [join_selected(r, '::text') for r in elem.css(tag)]


def to_int(val):
    return int(re.sub('\D', '', val))


def to_float(val):
    return float(val.replace(',', '.'))
