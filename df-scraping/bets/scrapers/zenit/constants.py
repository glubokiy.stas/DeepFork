from bets import constants as c

PAGE_SIZE = 50
TABLE_URL = (
    'https://zenitbet.com/ajax/line/printer/table'
    '?sport={sport}'
    '&ross=1'
    '&length=' + str(PAGE_SIZE) +
    '&offset={offset}'
    '&timezone_id=3'
    '&lang_id=1'
)
COOKIES = {
    'timezone': 36
}

SPORTS_MAPPING = {
    'Футбол': c.SOCCER,
    'Хоккей': c.ICE_HOCKEY,
    'Баскетбол': c.BASKETBALL,
    'Теннис': c.TENNIS,
    'Волейбол': c.VOLLEYBALL,
}

HANDICAP1_VAL = 'Ф1'
HANDICAP2_VAL = 'Ф2'
OVERUNDER_VAL = 'Тот'

HANDICAP1 = 'Кф1'
HANDICAP2 = 'Кф2'
OVER = 'Бол'
UNDER = 'Мен'
BOTH_SCORE = 'Обе забьют'
NOT_BOTH_SCORE = 'Одна не забьет'

BET_TO_VAL_MAPPING = {
    HANDICAP1: HANDICAP1_VAL,
    HANDICAP2: HANDICAP2_VAL,
    OVER: OVERUNDER_VAL,
    UNDER: OVERUNDER_VAL,
}

BET_MAPPING = {
    'П1': c.W1,
    'П2': c.W2,
    'Х': c.X,
    '1Х': c.W1X,
    'Х2': c.W2X,
    '12': c.W1W2,
    BOTH_SCORE: c.BOTH_SCORE,
    NOT_BOTH_SCORE: c.NOT_BOTH_SCORE,
    HANDICAP1: c.HANDICAP1,
    HANDICAP2: c.HANDICAP2,
    OVER: c.OVER,
    UNDER: c.UNDER,
}

PART_MAPPING = {
    'сет': c.SET,
    'партия': c.SET,
    'тайм': c.HALF,
    'половина': c.HALF,
    'четверть': c.QUARTER,
    'период': c.PERIOD,
}

STATS_MAPPING = {
    'ж/к': c.BY_YELLOW_CARDS,
    'офс': c.BY_OFFSIDES,
    'угл': c.BY_CORNERS,
    'уд в ст': c.BY_SHOTS_ON_GOAL,
    'уд по воротам': c.BY_SHOTS,
    'фолы': c.BY_FOULS,
    '% вл мяч': c.BY_BALL_POSS,
}
