import json
from datetime import datetime
from time import time

import scrapy

from bets.items import BetItem
from bets import constants as c
from bets.scrapers.zenit import constants as const


class ZenitSpider(scrapy.Spider):
    name = 'zenit'
    bookmaker = 'zenit'
    lang = 'ru'

    def start_requests(self):
        url = f'https://zenitbet.com/ajax/live/printer/react/{int(time())}?ross=1'
        yield scrapy.Request(url, cookies=const.COOKIES)

    def parse(self, response):
        data = json.loads(response.text)
        sports = data['dict']['sport']
        leagues = data['dict']['league']
        teams = data['dict']['cmd']

        for game in data['games'].values():
            sport = const.SPORTS_MAPPING.get(sports[str(game['sid'])])

            if sport is None:
                continue

            base_item = BetItem().extend(
                sport=sport,
                league=leagues[str(game['lid'])],
                p1=teams[str(game['c1_id'])],
                p2=teams[str(game['c2_id'])],
                datetime=str(datetime.fromtimestamp(game['time']))
            )


