from datetime import datetime as dt

import scrapy
from scrapy import Request
from scrapy.exceptions import DropItem

import bets.constants as c
import bets.scrapers.zenit.constants as const
from bets.items import BetItem
from bets.utils import get_row_text, to_float, to_int


class ZenitSpider(scrapy.Spider):
    name = '_zenit'
    bookmaker = 'zenit'
    lang = 'ru'

    year = dt.now().year

    custom_settings = {
        'DOWNLOAD_DELAY': 0.3,
    }

    def start_requests(self):
        for sport_id in const.SPORTS_MAPPING:
            meta = {
                'sport': sport_id,
                'offset': 0,
            }
            yield Request(
                const.TABLE_URL.format(**meta),
                meta=meta,
                cookies=const.COOKIES
            )

    def parse(self, response):
        sport = const.SPORTS_MAPPING[response.meta['sport']]

        self.logger.info(f'Scraping: {sport} (offset {response.meta["offset"]})')

        league_tables = response.css('table.l-t')
        for league_table in league_tables:
            league_name = league_table.css('tbody tr:first-child th span:nth-child(2)::text').get()

            col_names = get_row_text(league_table.css('.h-tr')[0], 'th')
            match_ids = league_table.css('tr[id^="g-tr-"]::attr(id)').re('g-tr-(\d+)')

            for match_id in match_ids:
                first_row_box = league_table.css(f'#g-tr-{match_id}')
                bet_dt_raw = first_row_box.css('.g-dt-td::text').get()
                bet_dt = str(dt.strptime(bet_dt_raw, '%d/%m %H:%M').replace(year=self.year))
                players = first_row_box.css('.g-d nobr::text').getall()

                if len(players) != 2:
                    break

                base_item = BetItem()

                base_item['sport'] = sport
                base_item['league'] = league_name.split('. ', 1)[1]
                base_item['datetime'] = bet_dt
                base_item['orig_p1'] = players[0]
                base_item['orig_p2'] = players[1]

                for stat, value in const.STATS_MAPPING.items():
                    if base_item['orig_p1'].endswith(f' {stat}'):
                        base_item['extra'] = value
                        break

                # Parsing first row

                first_row_map = dict(zip(col_names, get_row_text(first_row_box)))
                yield from self._parse_row(first_row_map, base_item)

                # Parsing additional rows for different game parts (quarters, halves)

                for row_box in league_table.css(f'.g-tr.g-a-tr-{match_id}'):
                    row_values = get_row_text(row_box)
                    row_len = len(row_values)
                    row_map = dict(zip(col_names[-row_len:], row_values))

                    yield from self._parse_row(row_map, base_item, row_values[0])

                # Parsing extended section

                for table in league_table.css(f'#g-a-tr-{match_id} table'):
                    yield from self._parse_table(table, base_item)

        if league_tables:
            for i in range(1, 6):
                new_meta = {
                    'sport': response.meta['sport'],
                    'offset': response.meta['offset'] + const.PAGE_SIZE
                }
                yield Request(
                    const.TABLE_URL.format(**new_meta),
                    meta=new_meta,
                    cookies=const.COOKIES
                )

    def _parse_row(self, row_map, base_item, part_raw=None):
        for bet, coef in row_map.items():
            if bet not in const.BET_MAPPING or not coef:
                continue

            item = base_item.extend(bet=bet, coef=coef)

            if bet in const.BET_TO_VAL_MAPPING:
                item['value'] = to_float(row_map[const.BET_TO_VAL_MAPPING[bet]])

            if part_raw is not None:
                part_number, part = part_raw.split()
                item['part_number'] = to_int(part_number)
                item['part'] = const.PART_MAPPING[part]

            yield item

    def _parse_table(self, table, base_item):
        header = get_row_text(table.css('thead tr'), 'th')[0]

        if header == 'Основное время':
            for tr in table.css('tbody tr'):
                row_values = get_row_text(tr)

                for k, v in zip(row_values[::2], row_values[1::2]):
                    yield base_item.extend(bet=k, coef=v, part=c.WITHOUT_OVERTIME)

        elif header == 'Тотал':
            for tr in table.css('tbody tr'):
                row_values = get_row_text(tr)

                if len(row_values) == 4:
                    yield base_item.extend(bet=const.UNDER, coef=row_values[1], value=to_float(row_values[0]))
                    yield base_item.extend(bet=const.OVER, coef=row_values[3], value=to_float(row_values[2]))

                elif len(row_values) == 6:
                    for v, u, o in zip(row_values[::3], row_values[1::3], row_values[2::3]):
                        if v:
                            yield base_item.extend(bet=const.UNDER, coef=u, value=to_float(v))
                            yield base_item.extend(bet=const.OVER, coef=o, value=to_float(v))

        elif header == 'Фора 1':
            for tr in table.css('tbody tr'):
                row_values = get_row_text(tr)

                yield base_item.extend(bet=const.HANDICAP1, coef=row_values[1], value=to_float(row_values[0]))
                yield base_item.extend(bet=const.HANDICAP2, coef=row_values[3], value=to_float(row_values[2]))

        elif header == 'Фора 1 по партиям' or header == 'Фора 1 по сетам':
            for tr in table.css('tbody tr'):
                row_values = get_row_text(tr)

                yield base_item.extend(
                    bet=const.HANDICAP1,
                    extra=c.BY_SETS,
                    coef=row_values[1],
                    value=to_float(row_values[0])
                )

                yield base_item.extend(
                    bet=const.HANDICAP2,
                    extra=c.BY_SETS,
                    coef=row_values[3],
                    value=to_float(row_values[2])
                )

        elif header == 'Обе забьют':
            row_values = get_row_text(table.css('tbody tr')[0])

            yield base_item.extend(bet=const.BOTH_SCORE, coef=row_values[1])
            yield base_item.extend(bet=const.NOT_BOTH_SCORE, coef=row_values[3])

    @staticmethod
    def clean_bet(val):
        if val in const.BET_MAPPING:
            return const.BET_MAPPING[val]

        raise DropItem()

    @staticmethod
    def clean_coef(val):
        if val == '':
            raise DropItem()

        return to_float(val)
