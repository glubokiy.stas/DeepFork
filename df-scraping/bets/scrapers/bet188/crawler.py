import json

import scrapy

import bets.constants as c
import bets.scrapers.bet188.constants as const
from bets.items import BetItem


class Bet188Spider(scrapy.Spider):
    name = 'bet188'
    bookmaker = '188bet'

    custom_settings = {
        'DOWNLOAD_DELAY': 0.4,
    }

    seen_extras = set()

    def start_requests(self):
        for sport in const.SPORTS_MAPPING:
            meta = {'sport': sport, 'page': 1}
            formdata = const.FORMDATA.copy()
            formdata['reqUrl'] = const.REQ_URL.format(**meta)
            yield scrapy.FormRequest(const.GETDATA_URL, formdata=formdata, meta=meta, cookies=const.COOKIES)

    def parse(self, response):
        data = json.loads(response.text)
        sport = const.SPORTS_MAPPING[response.meta['sport']]
        page = response.meta['page']

        self.logger.info(f'Scraping: {sport} (page {page})')

        for league in data['mod']['d'][0]['c']:
            league_name = league['n']

            for match in league['e']:
                if 'o' not in match:
                    continue

                subtitle = match['cei']['n']
                if subtitle and subtitle not in const.EXTRAS:
                    if subtitle not in self.seen_extras:
                        self.logger.warn(f'Unknown extra: {subtitle}')
                        self.seen_extras.add(subtitle)
                    continue

                base_item = BetItem()

                if subtitle:
                    base_item['extra'] = const.EXTRAS[subtitle]

                base_item['sport'] = sport
                base_item['league'] = league_name
                base_item['orig_p1'] = match['i'][0]
                base_item['orig_p2'] = match['i'][1]
                base_item['datetime'] = match['edt'].replace('T', ' ')

                for bet_label, vals in match['o'].items():
                    if bet_label.startswith('1x2') and sport == 'basketball':
                        self.logger.warning(f'No handling for ({bet_label}, {sport}) pair')
                        continue

                    for item in self._parse_odds(bet_label, vals, base_item):
                        for suffix, part_with_number in const.PART_MAPPING.items():
                            if bet_label.endswith(suffix):
                                item['part'], item['part_number'] = part_with_number

                        if bet_label in ['ah', 'ou'] and sport in['tennis', 'volleyball']:
                            item['extra'] = c.BY_SETS

                        yield item

        if page < data['mod']['p']['t']:
            meta = {'sport': response.meta['sport'], 'page': page + 1}
            formdata = const.FORMDATA.copy()
            formdata['reqUrl'] = const.REQ_URL.format(**meta)
            yield scrapy.FormRequest(const.GETDATA_URL, formdata=formdata, meta=meta)

    def _parse_odds(self, bet_label, vals, base_item: BetItem):
        if bet_label in self.key_options('1x2'):
            for bet, coef in zip([c.W1, c.W2, c.X], vals[1::2]):
                yield base_item.extend(bet=bet, coef=coef)

        if bet_label in self.key_options('ml'):
            for bet, coef in zip([c.W1, c.W2], vals[1::2]):
                yield base_item.extend(bet=bet, coef=coef)

        if bet_label in self.key_options('ah'):
            yield from self.fill_item_ah_ou(base_item.extend(bet=c.HANDICAP1), dict(zip(vals[1::8], vals[5::8])))
            yield from self.fill_item_ah_ou(base_item.extend(bet=c.HANDICAP2), dict(zip(vals[3::8], vals[7::8])))

        if bet_label in self.key_options('ou'):
            yield from self.fill_item_ah_ou(base_item.extend(bet=c.OVER), dict(zip(vals[1::8], vals[5::8])))
            yield from self.fill_item_ah_ou(base_item.extend(bet=c.UNDER), dict(zip(vals[3::8], vals[7::8])))

        if bet_label in self.key_options('bts'):
            for both_to_score, _, coef in vals:
                bet = c.BOTH_SCORE if both_to_score == 'Yes' else c.NOT_BOTH_SCORE
                yield base_item.extend(bet=bet, coef=coef)

    def fill_item_ah_ou(self, base_item, mapping):
        for value, coef in mapping.items():
            yield base_item.extend(coef=coef, value=self.asian_to_decimal(value))

    @staticmethod
    def key_options(base):
        suffixes = ['', 'pt', 'fts'] + const.PART_SUFFIXES
        return [base + suffix for suffix in suffixes]

    @staticmethod
    def asian_to_decimal(val):
        if '/' in val:
            q = sum(abs(float(x)) for x in val.split('/')) / 2
            if val.startswith('-'):
                q = -q
            return q

        return float(val)

    @staticmethod
    def clean_coef(val):
        return float(val)
