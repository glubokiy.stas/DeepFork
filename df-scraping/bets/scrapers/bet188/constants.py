from bets import constants as c

GETDATA_URL = 'https://landing-sb.prdasbb18a1.com/en-gb/Service/CentralService?GetData'
REQ_URL = '/en-gb/sports/{sport}/competition/full-time-asian-handicap-and-over-under?tzoff=0&pageno={page}'
FORMDATA = {
    'IsFirstLoad': 'true',
    'VersionL': '-1',
    'VersionU': '0',
    'VersionS': '-1',
    'VersionF': '-1',
    'VersionH': '0',
    'VersionT': '-1',
    'IsEventMenu': 'false',
    'SportID': '1',
    'CompetitionID': '-1',
    'oIsInplayAll': 'false',
    'oIsFirstLoad': 'true',
    'oSortBy': '1',
    'oOddsType': '0',
    'oPageNo': '0',
    'LiveCenterEventId': '0',
    'LiveCenterSportId': '0',
}
COOKIES = {
    'timeZone': 0
}

SPORTS_MAPPING = {
    'football': c.SOCCER,
    'basketball': c.BASKETBALL,
    'ice-hockey': c.ICE_HOCKEY,
    'tennis': c.TENNIS,
    'volleyball': c.VOLLEYBALL,
}

PART_MAPPING = {
    '1st': (c.HALF, 1),
    '2nd': (c.HALF, 2),
    's1': (c.SET, 1),
    's2': (c.SET, 2),
    's3': (c.SET, 3),
    'q1': (c.QUARTER, 1),
    'q2': (c.QUARTER, 2),
    'q3': (c.QUARTER, 3),
    'q4': (c.QUARTER, 4),
    'ot': (c.WITH_OVERTIME, None),
}

PART_SUFFIXES = list(PART_MAPPING)

EXTRAS = {
    'Corners': c.BY_CORNERS,
}
