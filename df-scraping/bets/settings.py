import os

BOT_NAME = 'bets'

SCRAPERS_DIR = os.path.join(os.path.dirname(__file__), 'scrapers')
SPIDER_MODULES = [
    f'bets.scrapers.{f}'
    for f in os.listdir(SCRAPERS_DIR)
    if os.path.isdir(os.path.join(SCRAPERS_DIR, f))
]

DOWNLOAD_DELAY = 3

LOG_LEVEL = 'INFO'

DOWNLOADER_MIDDLEWARES = {
    'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
    'scrapy_fake_useragent.middleware.RandomUserAgentMiddleware': 400,
}

ITEM_PIPELINES = {
    'bets.pipelines.CleanPipeline': 100,
}

LOG_FORMATTER = 'bets.logformatter.DFLogFormatter'

FEED_URI = os.path.join(os.path.dirname(__file__), 'local_output', '%(name)s', 'data-%(time)s.csv')
FEED_FORMAT = 'csv'
