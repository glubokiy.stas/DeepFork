import scrapy


class BaseItem(scrapy.Item):

    def extend(self, **kwargs):
        copy = self.copy()
        for k, v in kwargs.items():
            copy[k] = v
        return copy


class BetItem(BaseItem):
    bookmaker = scrapy.Field()
    lang = scrapy.Field()

    sport = scrapy.Field()
    league = scrapy.Field()
    datetime = scrapy.Field()
    p1 = scrapy.Field()
    p2 = scrapy.Field()

    bet = scrapy.Field()
    coef = scrapy.Field()
    value = scrapy.Field()

    part = scrapy.Field()
    part_number = scrapy.Field()

    extra = scrapy.Field()
