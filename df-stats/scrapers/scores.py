import functools
import json
import random
import re
from datetime import datetime

import scrapy
from Levenshtein import ratio

CSE_URL = (
    'https://www.googleapis.com/customsearch/v1'
    '?key={key}'
    '&cx={cse}'
    '&q={q}'
)
SOFASCORE_EVENT_URL = 'https://www.sofascore.com/event/{}/json'
SOFASCORE_H2H_URL = 'https://www.sofascore.com/event/{}/matches/json'
SOFASCORE_TEAM_EVENTS_URL = 'https://www.sofascore.com/team/{}/events/json'
CSE = '001711700838735485911:t0rsfpnygqk'

HOUR_DIFF_THRESHOLD = 2
GROUPING_SIMILARITY_THRESHOLD = 0.7
SCRAPING_SIMILARITY_THRESHOLD = 0.55

SOFASCORE_SPORT_MAP = {
    'football': 'Футбол'
}


def memorize(f):
    """Add each url to meta 'visited' entry to know which pages were already checked for this match."""
    @functools.wraps(f)
    def wrapper(self, response):
        response.meta['visited'].add(response.url)

        return f(self, response)

    return wrapper


class ScoresSpider(scrapy.Spider):
    """Spider for scores from sofascore and 777score.

    Due to different team spelling on different sites, we cannot rely on score sites search.
    That's why Google Search Engine is used (because it is good at dealing with misspelling).

    The flow is the following:
    1. Make a request for Google Search in format: "Team1 - Team2"
    2. Get the url from search results that was not visited before
    3. Sofascore: step 4. 777score: step 5.
    4. Sofascore scraping: Match -> H2H -> Team
    5. 777 score scraping: Match -> H2H; Tournament -> Match; Team -> Match
    6. If there are links from search left, go to step 2
    """

    name = 'scores'

    custom_settings = {
        'LOG_LEVEL': 'WARNING',
        'DOWNLOAD_DELAY': 0.3,
    }

    # Google has limitation to only 100 requests per project per day for Custom Search Engine
    # 13 projects are used thus giving 1300 requests per day which should be enough
    api_keys = {
        'AIzaSyBQrP5H7NH01nXXWjwwsChbjc1QG-jjHIY',  # DeepFork
        'AIzaSyDvdO3F9vlwYVCCENsRY-4-vSzIO3RqSZY',  # DeepFork-2
        'AIzaSyARL3mVoZapwNB4DZK8ZCHjwjnXd8kXbzw',  # DeepFork-3
        'AIzaSyD-jg3gwwToO_JeIOKw4PKGj-rxljKeFt4',  # DeepFork-4
        'AIzaSyAZAGAcvstxnE84wmw0U-e11EtJ_TTQc6c',  # DeepFork-5
        'AIzaSyC-PwybFyp516iCDMbWA0_H9gqF01Otz1w',  # DeepFork-6
        'AIzaSyDIW1l7gwveACms2odHV6Wy1Jnf19BTXuE',  # DeepFork-7
        'AIzaSyB_wl8K3QCXmCS9rJ5cZnkmpgz_IP42_NI',  # DeepFork-8
        'AIzaSyDB1BEsI00DiVNNpDik0ZGs9SQ5Jrofea4',  # DeepFork-9
        'AIzaSyBaYEujWxGSxFDFYr7UNEUBZmSWU3UNBn4',  # DeepFork-10
        'AIzaSyDMuLDHLmbaGW_ymkUJKyOnkcv0ViatxNc',  # Dronnet
        'AIzaSyCLN3HLlztYy0fS-g6HxoRxINxjKidT3go',  # OriginalParts
        'AIzaSyB62KNhc8j_BNbEHFZP4Hf_h8QFdDg-iVo',  # LinguaLeo Uploader
        'AIzaSyDyZrvRaHSUjSK1RWe2eB2HgO0HKEpIIGA',  # Kubescrapes
    }
    year = datetime.now().year
    data = []

    def start_requests(self):
        """
        Group matches to reduce number of requests to be sent and then request Google Search for sofascore.
        Still, we cannot guarantee that there will not be false negative in grouping with Levenstein ratio.
        As an example, imagine "PSG" in one match and "Paris Saint-Germain F.C." in other match.
        That's why we may request some matches several times and thus we need to use dont_filter=True for requests.
        """
        self.logger.info(f'To process: {len(self.data)} rows.')
        unique_matches = get_unique_matches(self.data)
        self.logger.info(f'After grouping: {len(unique_matches)} matches left')

        for matches in unique_matches:
            yield self.request_google_search(matches)

    def retry_google(self, failure):
        """Errback for likely case of reaching quota for the API key and getting 403 error."""
        meta = failure.request.meta

        # If google search failed, then it is likely that quota is reached and a different key has to be used
        if meta['key'] in self.api_keys:
            self.logger.warning(f'API key is no longer valid: {meta["key"]}')
            self.api_keys.remove(meta['key'])

        # If no keys are left, then we can't do the search and thus cannot proceed to parsing the scores pages
        if not self.api_keys:
            return

        return self.request_google_search(meta['matches'])

    def parse_google(self, response):
        """Parse results from Custom Search Engine API."""
        d = json.loads(response.text)

        if not d.get('items'):
            return self.on_failure(response)

        valid_links = [item['link'] for item in d['items'] if is_valid_link(item['link'])]

        self.logger.debug(f'Google for "{response.meta["query"]}": {len(valid_links)} results')

        if not valid_links:
            return self.on_failure(response)

        return self.request_page_from_results(response.meta, valid_links)

    @memorize
    def parse_sofascore_page(self, response):
        """
        Parse sofascore page. As the page is filled with data from sofascore API, we need to request it after
        extracting the event ID.
        """
        event_id = response.css('span[data-event-id]::attr(data-event-id)').get()

        if event_id is None:
            self.logger.debug(f'No event id found: {response.url}')
            return self.on_failure(response)

        url = SOFASCORE_EVENT_URL.format(event_id)

        if url in response.meta['visited']:
            return self.on_failure(response)

        return scrapy.Request(url, self.parse_sofascore_event, dont_filter=True, meta={
            'matches': response.meta['matches'],
            'visited': response.meta['visited'],
            'extra_links': response.meta['extra_links']
        })

    @memorize
    def parse_sofascore_event(self, response):
        """
        Parse data from sofascore event API. If the date of the game does not match the actual date,
        try to find the necessary game from head-to-head games between the teams.
        """
        d = json.loads(response.text)
        event = d['event']

        success = self.fill_sofascore_event(response, [event])

        # If the event is not the necessary one, try h2h games
        if not success:
            url = SOFASCORE_H2H_URL.format(event['id'])

            if url in response.meta['visited']:
                return self.on_failure(response)

            return scrapy.Request(url, self.parse_sofascore_h2h, dont_filter=True, meta={
                'matches': response.meta['matches'],
                'visited': response.meta['visited'],
                'extra_links': response.meta['extra_links']
            })

    @memorize
    def parse_sofascore_h2h(self, response):
        """
        Parse head-to-head games between two teams to find if any of the games matches the date.
        If none of the events matches the actual date, try to find the game from recent team games.
        It may be useful in the case if Google Search initially gave a link to a game between two teams, one of which is
        a valid one and the other is not.
        """
        d = json.loads(response.text)

        events = [event for t in d['h2h']['events']['tournaments'] for event in t['events']]
        success = self.fill_sofascore_event(response, events)

        # If cannot find the necessary event, try to look at teams recent games
        if not success:
            away_url = SOFASCORE_TEAM_EVENTS_URL.format(d['away']['team']['id'])
            home_url = SOFASCORE_TEAM_EVENTS_URL.format(d['home']['team']['id'])

            if home_url in response.meta['visited']:
                response.meta['extra_links'] = [away_url] + response.meta['extra_links']
                return self.on_failure(response)

            return scrapy.Request(home_url, self.parse_sofascore_team, dont_filter=True, meta={
                'matches': response.meta['matches'],
                'visited': response.meta['visited'],
                # Add URL for away team to the queue to not split requesting to avoid many dupe requests
                'extra_links': [away_url] + response.meta['extra_links']
            })

    @memorize
    def parse_sofascore_team(self, response):
        """Parse recent team games to find if any of the games matches the date."""
        d = json.loads(response.text)

        events = [event for t in d['tournaments'] for event in t['events']]
        success = self.fill_sofascore_event(response, events)

        if not success:
            return self.on_failure(response)

    def fill_sofascore_event(self, response, events):
        """
        From a list of events, find the one that matches the necessary game.
        Returns True if the game was found (even if canceled).
        """
        row = response.meta['matches'][0]
        match_time = row['datetime']

        for event in events:
            if SOFASCORE_SPORT_MAP.get(event['sport']['slug']) != row['sport']:
                continue

            event_url = SOFASCORE_EVENT_URL.format(event['id'])

            if match_ratio(row['event'], event['name']) < SCRAPING_SIMILARITY_THRESHOLD:
                continue

            # Find hour difference between actual game time and the time found on the page
            hour_diff = hour_diff_timestamp_time(event['startTimestamp'], match_time)
            if hour_diff > HOUR_DIFF_THRESHOLD:
                continue

            # If the game was canceled, mark it as such
            if event.get('statusDescription') == 'Canceled' or event.get('statusDescription') == 'Postponed':
                for row in response.meta['matches']:
                    row['score'] = {
                        'status': 'Canceled',
                        'url': event_url
                    }
                self.logger.info(f'Match canceled: {row["event"]} ({event_url})')
                return True

            if event.get('statusDescription') == 'Coverage canceled':
                continue

            # If no score is found, need to look into the case closer, as it is not expected
            if event['homeScore'].get('normaltime') is None:
                self.logger.warn(f'UNEXPECTED! Score is not found: {event_url}')
                continue

            for row in response.meta['matches']:
                row['score'] = {
                    'home': {
                        'name': event['homeTeam']['name'],
                        'full': event['homeScore']['normaltime'],
                        'period1': event['homeScore'].get('period1')
                    },
                    'away': {
                        'name': event['awayTeam']['name'],
                        'full': event['awayScore']['normaltime'],
                        'period1': event['awayScore'].get('period1')
                    },
                    'url': event_url
                }

            self.logger.info(f'Got score for {row["event"]} ({event_url})')
            return True

        return False

    @memorize
    def parse_777score_event(self, response):
        """Parse 777score event page."""
        row = response.meta['matches'][0]
        match_time = row['datetime']

        if response.css('span.iconPenaltyShootout') or response.css('span.iconExtraTime'):
            # If there was extra time or penalty shootout in the match, it may contain incorrect scores
            return self.on_failure(response)

        teams = response.css('.team-name::text').getall()

        if not teams:
            return self.on_failure(response)

        team1, team2 = teams

        if match_ratio(row['event'], f'{team1} - {team2}') < SCRAPING_SIMILARITY_THRESHOLD:
            return self.on_failure(response)

        str_time = response.css('.date::text').get() + ' ' + response.css('.time::text').get()
        hour_diff = hour_diff_str_time(str_time, match_time)

        if hour_diff > HOUR_DIFF_THRESHOLD:
            return self.find_777score_h2h_match(response)

        if response.css('.l-label::text').get() == 'Canceled':
            for row in response.meta['matches']:
                row['score'] = {
                    'status': 'Canceled',
                    'url': response.url
                }
            self.logger.info(f'Match canceled: {row["event"]} ({response.url})')
            return

        ft_score = [int(x) for x in response.css('.count').xpath('.//text()').getall() if x.isnumeric()]

        if ft_score:
            ft_score1, ft_score2 = ft_score
        else:
            return self.on_failure(response)

        first_half_comment = next((x for x in response.css('.comment::text').getall() if 'First Half Score' in x), None)

        if first_half_comment:
            p1_score1, p1_score2 = [int(x) for x in first_half_comment.split(' ') if x.isnumeric()]
        else:
            p1_score1, p1_score2 = None, None

        for row in response.meta['matches']:
            row['score'] = {
                'home': {
                    'name': team1,
                    'full': ft_score1,
                    'period1': p1_score1
                },
                'away': {
                    'name': team2,
                    'full': ft_score2,
                    'period1': p1_score2
                },
                'url': response.url
            }

        self.logger.info(f'Got score for {row["event"]} ({response.url})')

    def find_777score_h2h_match(self, response):
        row = response.meta['matches'][0]
        match_time = row['datetime']

        matches = response.css('ul.h2h .table .match')

        for match in matches:
            str_time = ' '.join(match.css('.date span::text').getall()) + f'/{self.year}'

            if not str_time:
                continue

            hour_diff = hour_diff_str_time(str_time, match_time, fmt='%H:%M %d/%m/%Y')
            if hour_diff <= HOUR_DIFF_THRESHOLD:
                url = response.urljoin(match.css('a::attr(href)').get())

                if url in response.meta['visited']:
                    continue

                return scrapy.Request(url, self.parse_777score_event, dont_filter=True, meta={
                    'matches': response.meta['matches'],
                    'visited': response.meta['visited'],
                    'extra_link': response.meta['extra_links']
                })

        return self.on_failure(response)

    @memorize
    def parse_777score_tournament(self, response):
        row = response.meta['matches'][0]
        match_time = row['datetime']

        matches = response.xpath('//li[contains(text(),"results")]/../ul/div/li')

        for match in matches:
            str_time = ' '.join(match.css('.date-time span::text').getall())

            if not str_time:
                continue

            hour_diff = hour_diff_str_time(str_time, match_time)
            if hour_diff > HOUR_DIFF_THRESHOLD:
                continue

            team1, team2 = match.css('.team span::text').getall()

            if match_ratio(row['event'], f'{team1} - {team2}') < SCRAPING_SIMILARITY_THRESHOLD:
                continue

            url = response.urljoin(match.css('a::attr(href)').get())

            if url in response.meta['visited']:
                continue

            return scrapy.Request(url, self.parse_777score_event, dont_filter=True, meta={
                'matches': response.meta['matches'],
                'visited': response.meta['visited'],
                'extra_link': response.meta['extra_links']
            })

        return self.on_failure(response)

    @memorize
    def parse_777score_team(self, response):
        row = response.meta['matches'][0]
        match_time = row['datetime']

        matches = response.css('div[role=tabpanel]')[0].css('ul')[1].css('li')[2:]

        for match in matches:
            str_time = ' '.join(match.css('.date-time span::text').getall())

            if not str_time:
                continue

            hour_diff = hour_diff_str_time(str_time, match_time)
            if hour_diff <= HOUR_DIFF_THRESHOLD:
                url = response.urljoin(match.css('a::attr(href)').get())

                if url in response.meta['visited']:
                    continue

                return scrapy.Request(url, self.parse_777score_event, dont_filter=True, meta={
                    'matches': response.meta['matches'],
                    'visited': response.meta['visited'],
                    'extra_link': response.meta['extra_links']
                })

        return self.on_failure(response)

    @memorize
    def parse_livebet(self, response):
        row = response.meta['matches'][0]
        match_time = row['datetime']

        str_time = response.css('.gameInfo-start::text').get()

        if not str_time:
            return self.on_failure(response)

        str_time = re.sub('\d+(st|nd|rd|th)', lambda m: m.group()[:-2].zfill(2), str_time)

        hour_diff = hour_diff_str_time(str_time, match_time, '%B %d, %Y %H:%M')

        if hour_diff > HOUR_DIFF_THRESHOLD:
            return self.on_failure(response)

        t1 = next(x.strip() for x in response.css('.soccerInfo-teamName-team1::text').getall() if x.strip())
        t2 = next(x.strip() for x in response.css('.soccerInfo-teamName-team2::text').getall() if x.strip())

        if match_ratio(row['event'], f'{t1} - {t2}') < SCRAPING_SIMILARITY_THRESHOLD:
            return self.on_failure(response)

        try:
            ft_score1 = int(response.css('.soccerInfo-teamScore-team1 i::text').get())
            ft_score2 = int(response.css('.soccerInfo-teamScore-team1 i::text').get())
        except TypeError:
            self.logger.warn(f'No score: {response.url}')
            return self.on_failure(response)

        try:
            p1_score1 = int(response.css('.tableStatistics-table tr')[1].css('td')[1].css('::text').get())
            p1_score2 = int(response.css('.tableStatistics-table tr')[2].css('td')[1].css('::text').get())
        except TypeError:
            p1_score1 = None
            p1_score2 = None

        for row in response.meta['matches']:
            row['score'] = {
                'home': {
                    'name': t1,
                    'full': ft_score1,
                    'period1': p1_score1
                },
                'away': {
                    'name': t2,
                    'full': ft_score2,
                    'period1': p1_score2
                },
                'url': response.url
            }

        self.logger.info(f'Got score for {row["event"]} ({response.url})')

    def on_failure(self, response):
        """In case of some error, try a different link from google search or request a different site."""
        # Try other link from google search if there are any
        extra_links = response.meta.get('extra_links')
        self.logger.debug(f'Move onto the next link for: {response.meta["matches"][0]["event"]}')
        return self.request_page_from_results(response.meta, extra_links)

    def request_google_search(self, matches):
        row = matches[0]
        query = row['event']

        # Randomly choose an api key from list of available ones
        key = random.choice(tuple(self.api_keys))
        url = CSE_URL.format(key=key, cse=CSE, q=query)

        return scrapy.Request(url, self.parse_google, errback=self.retry_google, meta={
            'matches': matches,
            'query': query,
            'key': key,
        })

    def request_page_from_results(self, meta, links):
        if not links:
            match = meta['matches'][0]['event']
            self.logger.warn(f'Unable to find the game: {match}')

            for row in meta['matches']:
                row['score'] = None
            return

        url = links[0]
        extra_links = links[1:]

        if 'visited' in meta and url in meta['visited']:
            return self.request_page_from_results(meta, extra_links)

        if 'sofascore' in url:
            if is_sofascore_team_link(url) and url.endswith('/json'):
                callback = self.parse_sofascore_team
            elif is_sofascore_team_link(url):
                callback = self.parse_sofascore_team

                # It does not make sense to request team page, because there's not much useful information there.
                # It's better to request sofascore team events API
                team_id = url.split('/')[-1]
                url = SOFASCORE_TEAM_EVENTS_URL.format(team_id)
            else:
                # Google gives a lot of results to the same page but on different languages, so we detect it
                game_custom_id = '/'.join(url.split('/')[-2:])
                for visited_url in meta.get('visited', []):
                    if '/'.join(visited_url.split('/')[-2:]) == game_custom_id:
                        return self.request_page_from_results(meta, extra_links)

                callback = self.parse_sofascore_page
        elif '777score' in url:
            if '/teams/' in url:
                callback = self.parse_777score_team
            elif '/tournaments/' in url:
                callback = self.parse_777score_tournament
            elif '/matches/' in url:
                callback = self.parse_777score_event
            else:
                self.logger.warn(f'Unfamiliar 777score link: {url}')
                return self.request_page_from_results(meta, extra_links)
        # elif 'livebet.com' in url:
        #     callback = self.parse_livebet
        #     headers = {
        #         'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
        #                       'Chrome/58.0.3029.110 Safari/537.36 '
        #     }
        else:
            return self.request_page_from_results(meta, extra_links)

        return scrapy.Request(url, callback, dont_filter=True, meta={
            'matches': meta['matches'],
            'visited': meta.get('visited', set()),
            'extra_links': extra_links
        })


def get_unique_matches(data):
    """Group matches by name and date, using Levenstein ratio."""
    groups = {}

    for row in data:
        if row['datetime'] not in groups:
            groups[row['datetime']] = {}
            groups[row['datetime']][row['event']] = [row]
            continue

        matches_on_the_date = groups[row['datetime']]

        best_match = None
        best_ratio = None
        for m in matches_on_the_date:
            r = match_ratio(m, row['event'])
            if r > GROUPING_SIMILARITY_THRESHOLD and (best_ratio is None or r > best_ratio):
                best_match = m
                best_ratio = r

        if best_match is None:
            groups[row['datetime']][row['event']] = [row]
        else:
            groups[row['datetime']][best_match].append(row)

    return [matches for date_matches in groups.values() for matches in date_matches.values()]


def match_ratio(m1, m2):
    # Ignore matching games like PSG - Lion and PSG U19 - Lion U19 (these are different games)
    is_m1_young = re.search(r'U\d\d', m1)
    is_m2_young = re.search(r'U\d\d', m2)
    if (is_m1_young and not is_m2_young) or (is_m2_young and not is_m1_young):
        return 0

    # Replace en-dash with a hyphen
    m1 = m1.replace('–', '-')
    m2 = m2.replace('–', '-')

    # Swap teams so that "A - B" matches with "B - A"
    m1_alt = ' '.join(reversed(m1.split(' - ')))

    # Remove useless characters, UXX (U19/U20) labels and everything inside brackets
    m1, m2, m1_alt = [
        re.sub(r'([,/ ]|U\d+|\([^)]*\))', '', m).lower().replace(' - ', ' ')
        for m in [m1, m2, m1_alt]
    ]

    return max(ratio(m1, m2), ratio(m1_alt, m2))


def hour_diff_timestamp_time(timestamp, time):
    return abs((datetime.utcfromtimestamp(timestamp) - time).total_seconds()) / 3600


def hour_diff_str_time(str_time, time, fmt='%d.%m.%Y %H:%M'):
    return abs((datetime.strptime(str_time, fmt) - time).total_seconds()) / 3600


def is_valid_link(link):
    return not (link.endswith('.txt') or link.endswith('.xml'))


def is_sofascore_team_link(link):
    team_slugs = ['team', 'time', 'squadra', 'equipo', 'tim', 'equipe', 'druzyna', 'takim']
    return any(f'/{slug}/' in link for slug in team_slugs)
