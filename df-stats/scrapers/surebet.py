import json
import os
from datetime import datetime
from urllib.parse import quote

import scrapy
from parsel import Selector

from constants import BOOKMAKERS, SPORTS


class SureBetSpider(scrapy.Spider):
    name = 'surebet-valuebets'
    hours = 48
    start_urls = ['https://ru.surebet.com/valuebets']
    url_template = (
        'https://ru.surebet.com/valuebets'
        '?utf8=%E2%9C%93'
        '&selector%5Bmin_group_size%5D=10'
        '&selector%5Bsettled_in%5D={seconds}'
        '&selector%5Bbookies_settings%5D={bookies_query}'
        '&selector%5Bexclude_sports_ids_str%5D={sports_query}'
        '&order=overvalue_desc'
        '&narrow='
        '&page={page}'
    )
    custom_settings = {
        'FEED_URI': os.path.join(os.path.dirname(__file__), '..', 'data', '%(time)s.json'),
        'FEED_FORMAT': 'json',
        'FEED_EXPORT_ENCODING': 'utf-8',
        'LOG_LEVEL': 'WARNING',
    }
    cookies = {'timezoneOffset': '0'}

    def parse(self, response):
        bookies_html = response.css('#xbookies-block::attr(data-facebox-backup)').get()
        bookies_json = Selector(bookies_html).css('.sbk-menu button::attr(data-bk)').getall()
        bookies_data = [json.loads(x) for x in bookies_json]
        bookies_str = [('0' if x['bk'] not in BOOKMAKERS else '4') + f':{x["bookie_ref"]}::' for x in bookies_data]
        bookies_query = quote(';'.join(bookies_str))

        sports_html = response.css('#xsports-block::attr(data-facebox-backup)').get()
        sports_checkboxes = Selector(sports_html).css('input[type=checkbox]')
        sports_ids_filtered = [
            x.css('::attr(data-ref)').get()
            for x in sports_checkboxes
            if x.css('::attr(value)').get() not in SPORTS + ['sports_all']
        ]
        sports_query = '+'.join(sports_ids_filtered)

        seconds = int(self.hours) * 3600
        meta = {'seconds': seconds, 'page': 1, 'bookies_query': bookies_query, 'sports_query': sports_query}

        yield scrapy.Request(
            self.url_template.format(**meta),
            self.parse_surebets,
            cookies=self.cookies,
            meta=meta
        )

    def parse_surebets(self, response):
        self.logger.info(f'{response.url}')
        self.logger.info('Scraping page {page}'.format(**response.meta))

        for bet_box in response.css('.app-table tbody'):
            # Skip unfamiliar bookmakers (probably sponsored)
            bookie_url = bet_box.css('.booker a::attr(href)').get()
            if not any(bookie_code in bookie_url for bookie_code in BOOKMAKERS):
                continue

            bet = {
                'bookmaker': bet_box.css('.booker a::text').get(),
                'sport': bet_box.css('.booker .minor::text').get(),
                'datetime': self.get_date(' '.join(bet_box.css('.time::text').getall())),
                'event': bet_box.css('.event a::text').get(),
                'league': bet_box.css('.event .minor::text').get(),
                'bet': ' '.join(bet_box.css('.coeff').css('::text').getall()),
                'odds': float(bet_box.css('.value').css('::text').get()),
                'overprice': float(bet_box.css('td')[-2].css('::text').get()[:-1]),
            }

            bet['league'] = (
                bet['league']
                .replace('Football', '')
                .replace('All Competitions', '')
                .replace('Soccer', '')
                .strip(' /-')
            )

            self.output.append(bet)

            yield bet

    @staticmethod
    def get_date(date_str):
        now = datetime.utcnow()

        date_no_year = datetime.strptime(date_str, '%d/%m %H:%M')
        current_year = now.year
        potential_years = [current_year - 1, current_year, current_year + 1]
        potential_dates = (date_no_year.replace(year=year) for year in potential_years)

        closest_date = min(potential_dates, key=lambda date: abs((now - date).total_seconds()))

        return closest_date
