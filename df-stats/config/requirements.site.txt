Flask==1.0.2
Jinja2==2.10.1
gunicorn==19.9.0
google-api-python-client==1.7.7
google-auth-httplib2==0.0.3
google-auth-oauthlib==0.2.0
pymongo==3.8.0
