#!/bin/bash

if [ "$DEBUG" = "True" ]; then
    echo WORKING IN DEV ENVIRONMENT
    gunicorn --bind 0.0.0.0:8002 --reload server:app
else
    echo WORKING IN PROD ENVIRONMENT
    gunicorn --bind 0.0.0.0:8002 server:app
fi
