from Levenshtein import ratio


def check_bets(data):
    checks = [
        FullTimeScore,
        DNB,
        OverUnder,
        Handicap,
        Total,
        EuroHandicap,
        Goals,
    ]

    data = [row for row in data if any(check.is_valid(row['bet']) for check in checks) and row.get('score')]

    for row in data:
        result = None

        if row['score'].get('status', '').lower() == 'canceled':
            result = 'P'
        else:
            if to_swap(row['event'], row['score']):
                row['score']['home'], row['score']['away'] = row['score']['away'], row['score']['home']

            for check in checks:
                if check.is_valid(row['bet']):
                    result = check.check(row['bet'], row['score'])
                    break

        row['scraped_res'] = result


def to_swap(match, score):
    act_team1, act_team2 = match.split(' – ')
    scr_team1, scr_team2 = score['home']['name'], score['away']['name']

    teams_not_swapped = ratio(act_team1, scr_team1) + ratio(act_team2, scr_team2)
    teams_are_swapped = ratio(act_team1, scr_team2) + ratio(act_team2, scr_team1)

    if teams_are_swapped > teams_not_swapped + 0.25:
        return True

    return False


class BaseChecker:

    @classmethod
    def is_valid(cls, bet):
        if bet.endswith('-я команда'):
            bet = bet[:-13].strip()

        if bet.endswith('-й период'):
            bet = bet[:-12].strip()

        if bet.endswith('90 минут'):
            bet = bet[:-10].strip()

        return cls.is_valid_base(bet)

    @classmethod
    def is_valid_base(cls, bet):
        raise NotImplementedError()

    @classmethod
    def check(cls, bet, score):
        s1 = score['home']['full']
        s2 = score['away']['full']

        if bet.endswith('-я команда'):
            team = bet[-11]
            bet = bet[:-13].strip()
        else:
            team = None

        if bet.endswith('-й период'):
            part = bet[-10]
            bet = bet[:-12].strip()

            if score['home']['period1'] is None or score['away']['period1'] is None:
                return None

            if part == '1':
                s1 = score['home']['period1']
                s2 = score['away']['period1']
            else:
                s1 = score['home']['full'] - score['home']['period1']
                s2 = score['away']['full'] - score['away']['period1']

        if bet.endswith('90 минут'):
            bet = bet[:-10].strip()

        if team is not None:
            if team == '1':
                s1 = s1
            else:
                s1 = s2
            s2 = None

        return cls.check_base(bet, s1, s2)

    @classmethod
    def check_base(cls, bet, s1, s2):
        raise NotImplementedError()


class FullTimeScore(BaseChecker):

    @classmethod
    def is_valid_base(cls, bet):
        return bet in ['1', 'X', '2', '1X', 'X2', '12']

    @classmethod
    def check_base(cls, bet, s1, s2):
        if bet == '1' and s1 > s2:
            return 'W'
        elif bet == '2' and s2 > s1:
            return 'W'
        elif bet == 'X' and s1 == s2:
            return 'W'
        elif bet == '1X' and s1 >= s2:
            return 'W'
        elif bet == 'X2' and s2 >= s1:
            return 'W'
        elif bet == '12' and s2 != s1:
            return 'W'

        return 'L'


class DNB(BaseChecker):

    @classmethod
    def is_valid_base(cls, bet):
        return '/ DNB' in bet

    @classmethod
    def check_base(cls, bet, s1, s2):
        if s1 == s2:
            return 'P'

        if bet.startswith('1') and s1 > s2:
            return 'W'
        elif bet.startswith('2') and s2 > s1:
            return 'W'

        return 'L'


class OverUnder(BaseChecker):

    @classmethod
    def is_valid_base(cls, bet):
        return ('Тб' in bet or 'Тм' in bet) and bet.endswith(')')

    @classmethod
    def check_base(cls, bet, s1, s2):
        val_str = bet[3:-1]
        check_val = float(val_str)
        over_under = 'over' if bet[1] == 'б' else 'under'

        actual_val = s1 if s2 is None else s1 + s2

        if '.' not in val_str:
            return cls.check_int(over_under, check_val, actual_val)

        if abs(check_val % 1 - 0.5) < 0.00001:
            return cls.check_half(over_under, check_val, actual_val)

        if abs(round(check_val) - check_val) - 0.25 < 0.00001:
            return cls.check_quarter(over_under, check_val, actual_val)

        raise ValueError(f'Unknown O/U: {bet}')

    @classmethod
    def check_int(cls, ou, c_val, a_val):
        c_val = int(round(c_val))

        if a_val == c_val:
            return 'P'

        return cls.check_half(ou, c_val, a_val)

    @classmethod
    def check_half(cls, ou, c_val, a_val):
        if ou == 'over' and a_val > c_val:
            return 'W'

        if ou == 'under' and a_val < c_val:
            return 'W'

        return 'L'

    @classmethod
    def check_quarter(cls, ou, c_val, a_val):
        c_val_int = round(c_val)
        c_val_half = c_val - 0.25 if c_val_int > c_val else c_val + 0.25

        i_res = cls.check_int(ou, c_val_int, a_val)
        h_res = cls.check_half(ou, c_val_half, a_val)

        if i_res == 'W' and h_res == 'W':
            return 'W'

        if i_res == 'L' and h_res == 'L':
            return 'L'

        if i_res == 'L' or h_res == 'L':
            return 'HL'

        if i_res == 'W' or h_res == 'W':
            return 'HW'


class Handicap(BaseChecker):

    @classmethod
    def is_valid_base(cls, bet):
        return bet.startswith('Ф') and bet.endswith(')')

    @classmethod
    def check_base(cls, bet, s1, s2):
        val_str = bet[3:-1].replace('−', '-')
        check_val = float(val_str)

        team = bet[1]

        if team == '1':
            s1 = s1 + check_val
        elif team == '2':
            s2 = s2 + check_val

        if '.' not in val_str:
            return cls.check_int(team, s1, s2)

        if abs(check_val % 1 - 0.5) < 0.00001:
            return cls.check_half(team, s1, s2)

        if abs(round(check_val) - check_val) - 0.25 < 0.00001:
            return cls.check_quarter(team, s1, s2)

        raise ValueError(f'Unknown handicap: {bet}')

    @classmethod
    def check_int(cls, team, s1, s2):
        s1 = round(s1)
        s2 = round(s2)

        if s1 == s2:
            return 'P'

        return cls.check_half(team, s1, s2)

    @classmethod
    def check_half(cls, team, s1, s2):
        if team == '1' and s1 > s2:
            return 'W'

        if team == '2' and s2 > s1:
            return 'W'

        return 'L'

    @classmethod
    def check_quarter(cls, team, s1, s2):
        if team == '1':
            s1_int = round(s1)
            s1_half = s1 - 0.25 if s1_int > s1 else s1 + 0.25
            args_list = [[s1_int, s2], [s1_half, s2]]
        else:
            s2_int = round(s2)
            s2_half = s2 - 0.25 if s2_int > s2 else s2 + 0.25
            args_list = [[s1, s2_int], [s1, s2_half]]

        i_res = cls.check_int(team, *args_list[0])
        h_res = cls.check_half(team, *args_list[1])

        if i_res == 'W' and h_res == 'W':
            return 'W'

        if i_res == 'L' and h_res == 'L':
            return 'L'

        if i_res == 'L' or h_res == 'L':
            return 'HL'

        if i_res == 'W' or h_res == 'W':
            return 'HW'


class Total(BaseChecker):

    @classmethod
    def is_valid_base(cls, bet):
        return 'Тотал ≥' in bet or 'Тотал ≤' in bet

    @classmethod
    def check_base(cls, bet, s1, s2):
        ou = 'over' if bet[6] == '≥' else 'under'
        check_val = int(bet[7:])

        actual_val = s1 if s2 is None else s1 + s2

        return OverUnder.check_half(ou, check_val - 0.5, actual_val)


class EuroHandicap(BaseChecker):

    @classmethod
    def is_valid_base(cls, bet):
        return any(bet.startswith(x) for x in ['1', '2']) and ':' in bet and bet.endswith(')')

    @classmethod
    def check_base(cls, bet, s1, s2):
        team = bet[0]
        plus1, plus2 = [int(x) for x in bet[2:-1].split(':')]

        s1 = s1 + plus1
        s2 = s2 + plus2

        return FullTimeScore.check_base(team, s1, s2)


class Goals(BaseChecker):

    @classmethod
    def is_valid_base(cls, bet):
        return bet.startswith('голы:')

    @classmethod
    def check_base(cls, bet, s1, s2):
        to_score = 'Будет' in bet
        actual_val = s1 if s2 is None else s1 + s2

        if to_score and actual_val > 0:
            return 'W'

        if not to_score and actual_val == 0:
            return 'W'

        return 'L'
