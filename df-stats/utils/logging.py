import json
import logging.config
import os

from constants import DEBUG


def setup_logging():
    with open(os.path.join(os.path.dirname(__file__), 'logging.json')) as f:
        logging_config = json.load(f)

    if DEBUG:
        for logger_name, logger_info in logging_config['loggers'].items():
            logger_info['level'] = 'DEBUG'

    logging.config.dictConfig(logging_config)
