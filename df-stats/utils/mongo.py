from os import environ

from pymongo import MongoClient as PymongoClient

from constants import MONGODB_DATABASE


class MongoClient:

    def __init__(self):
        self.client = PymongoClient('mongodb://{username}:{password}@{server}'.format(
            username=environ.get('MONGO_USERNAME'),
            password=environ.get('MONGO_PASSWORD'),
            server=environ.get('MONGO_SERVER')
        ))

        self.db = self.client[MONGODB_DATABASE]

    def __getattr__(self, item):
        return getattr(self.db, item)

    def get_record(self, collection_name):
        return self.db[collection_name].find_one()

    def update_record(self, collection_name, **kwargs):
        data = self.get_record(collection_name)

        if data:
            data = {**data, **kwargs}
        else:
            data = kwargs

        self.db[collection_name].replace_one({}, data, upsert=True)

    def sum_field(self, collection_name, field_name):
        pipe = [{'$group': {'_id': None, 'total': {'$sum': f'${field_name}'}}}]
        return list(self.db[collection_name].aggregate(pipe))[0]['total']


client = MongoClient()
