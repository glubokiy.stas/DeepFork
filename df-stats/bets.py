import glob
import hashlib
import json
import logging.config
import os
import time
from datetime import datetime

from scrapy.crawler import CrawlerProcess

from constants import SKIP_BETS, CLEAN_SCRAPED_AFTER_DAYS, DEBUG, START_BANK_VALUE
from scrapers.surebet import SureBetSpider
from utils.logging import setup_logging
from utils.mongo import client

logger = logging.getLogger('bets-script')
setup_logging()


def main():
    scraped_data = scrape()
    load_to_sheets(scraped_data)
    clean_old_files()


def scrape():
    process = CrawlerProcess(install_root_handler=False)

    # CrawlerProcess tries to override scrapy logging settings, need to set it back again.
    setup_logging()

    # The output will be stored in this variable
    scraped_data = []

    # noinspection PyTypeChecker
    process.crawl(SureBetSpider, output=scraped_data)
    process.start()

    return scraped_data


def get_data_files():
    return glob.glob(os.path.join(os.path.dirname(__file__), 'data', '*.json'))


def get_latest_scraped_data():
    latest_file = max(get_data_files(), key=os.path.getctime)
    with open(latest_file, encoding='utf-8') as f:
        data = json.load(f)

    return data


def clean_old_files():
    current_time = time.time()
    for f in get_data_files():
        creation_time = os.path.getctime(f)
        created_days_ago = (current_time - creation_time) // (24 * 3600)
        if created_days_ago > CLEAN_SCRAPED_AFTER_DAYS:
            os.unlink(f)
            logger.info(f'Old file "{f}" was removed')


def filter_bets(data):
    filtered = []

    for item in data:
        if not DEBUG:
            # Correct overprice to consider approximate bookmaker profit of 5-10%
            item['overprice'] = item['overprice'] / 2 - 3

            # Skip bets with low overprice value
            if item['overprice'] < 1:
                continue

            # Skip bets that are hard to check
            if any(bet in item['bet'] for bet in SKIP_BETS):
                continue

        # Skip bets that are already in the db
        if client.bets.find_one({'hash': get_hash(item)}):
            continue

        filtered.append(item)

    return filtered


def get_hash(item):
    s = item['event'] + str(item['datetime']) + item['bookmaker']
    return hashlib.sha1(s.encode('utf-8')).hexdigest()


def get_probability(item):
    # "Real" probability of the event
    return (item['overprice'] + 100) / item['odds']


def get_stake_pct(item):
    # Part of the bank to stake
    stake_pct = ((item['probability'] * item['odds']) / 100 - 1) / (item['odds'] - 1)

    return round(stake_pct, 4)


def get_stake(item):
    bank_record = client.get_record('bank')

    if bank_record:
        bank_value = bank_record['value']
    else:
        bank_value = START_BANK_VALUE

    stake = bank_value * item['stake_pct']

    updated_bank_value = round(bank_value - stake, 2)
    client.update_record('bank', value=updated_bank_value)

    return round(stake, 2)


def load_to_sheets(scraped_data):
    logger.info(f'Got {len(scraped_data)} records')
    data = filter_bets(scraped_data)
    logger.info(f'{len(data)} records left after filtering')

    if data:
        for item in data:
            item['hash'] = get_hash(item)
            item['probability'] = get_probability(item)
            item['stake_pct'] = get_stake_pct(item)
            item['stake'] = get_stake(item)
            item['bet_at'] = datetime.utcnow()

        # Create index if not exists
        hash_index_name = 'hash'
        client.bets.create_index(hash_index_name, unique=True)

        result = client.bets.insert_many(data)
        logger.info(f'{len(result.inserted_ids)} records inserted')

    client.update_record('site', last_bets_update=datetime.utcnow())


if __name__ == '__main__':
    main()

