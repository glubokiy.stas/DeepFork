import logging
from datetime import datetime

from flask import Flask, render_template

from constants import SITE_DIR
from utils.mongo import client as mongo
from utils.timer import Timer

app = Flask(__name__, template_folder=SITE_DIR)
app.config['TEMPLATES_AUTO_RELOAD'] = True

gunicorn_logger = logging.getLogger('gunicorn.error')
app.logger.handlers = gunicorn_logger.handlers
app.logger.setLevel(gunicorn_logger.level)


@app.route('/')
def home():
    with Timer() as t:
        data = mongo.get_record('site') or {}

        bank_record = mongo.get_record('bank')
        if bank_record:
            data['current_units'] = bank_record['value']

        data['count_without_result'] = mongo.bets.count_documents({'is_result_checked': {'$ne': True}})

    app.logger.info(f'Took {t.interval:.2f} seconds to fetch data from MongoDB')
    app.logger.info(f'Data: {data}')

    return render_template('home.jinja2', **data)


@app.route('/stats')
def stats():
    data = {
        'revenue': mongo.sum_field('bets', 'revenue'),
        'stake': mongo.sum_field('bets', 'stake'),
        'bets_count': mongo.bets.count_documents({})
    }

    return render_template('stats.jinja2', **data)


@app.template_filter()
def timesince(dt, default="just now"):
    """
    Returns string representing "time since" e.g.
    3 days ago, 5 hours ago etc.
    """
    now = datetime.utcnow()
    diff = now - dt

    periods = (
        (diff.days // 365, "year", "years"),
        (diff.days // 30, "month", "months"),
        (diff.days // 7, "week", "weeks"),
        (diff.days, "day", "days"),
        (diff.seconds // 3600, "hour", "hours"),
        (diff.seconds // 60, "minute", "minutes"),
        (diff.seconds, "second", "seconds"),
    )

    for period, singular, plural in periods:

        if period:
            return "%d %s ago" % (period, singular if period == 1 else plural)

    return default
