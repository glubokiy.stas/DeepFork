import logging
from datetime import datetime, timedelta

from scrapy.crawler import CrawlerProcess

from constants import START_BANK_VALUE
from scrapers.scores import ScoresSpider
from utils.checker import check_bets
from utils.logging import setup_logging
from utils.mongo import client

logger = logging.getLogger('bets-script')
setup_logging()


def main():
    data = get_events_to_check()

    logger.info(f'Got {len(data)} events to check')

    if not data:
        return

    scrape(data_to_scrape=data)

    check_bets(data)

    result_counter = 0
    for item in data:
        # If there is no score key on item, it means that the item was not checked
        if 'score' not in item:
            continue

        item.setdefault('scraped_res', 'Not Found')

        fields_to_add = {
            'is_result_checked': True,
            'result': item['scraped_res'],
            'profit': get_profit(item)
        }

        if fields_to_add['profit'] is None:
            fields_to_add['profit'] = 0

        fields_to_add['revenue'] = fields_to_add['profit'] + item['stake']

        # Add new fields to the item
        client.bets.update_one({'_id': item['_id']}, {'$set': fields_to_add})

        result_counter += 1

    logger.info(f'Scraped {result_counter}/{len(data)} results')

    # Update bank value
    total_stake = client.sum_field('bets', 'stake')
    total_revenue = client.sum_field('bets', 'revenue')
    total_profit = total_revenue - total_stake
    actual_bank_value = total_profit + START_BANK_VALUE
    client.update_record('bank', value=actual_bank_value)

    client.update_record('site', last_scores_update=datetime.utcnow())


def get_profit(item):
    if item['scraped_res'] in ['P', 'Not Found']:
        return 0

    if item['scraped_res'] == 'W':
        return (item['odds'] - 1) * item['stake']

    if item['scraped_res'] == 'L':
        return -item['stake']

    if item['scraped_res'] == 'HW':
        return (item['odds'] - 1) * item['stake'] / 2

    if item['scraped_res'] == 'HL':
        return -item['stake'] / 2

    logger.warning(f'Unknown result: {item["scraped_res"]} ({item})')


def get_events_to_check():
    time_two_hours_ago = datetime.utcnow() - timedelta(hours=2)

    # Find all bets that were not checked for result and are already 2+ hours in the past
    result = client.bets.find({
        'is_result_checked': {'$ne': True},
        'datetime': {'$lte': time_two_hours_ago}
    })

    return list(result)


def scrape(data_to_scrape):
    process = CrawlerProcess(install_root_handler=False)

    setup_logging()

    process.crawl(ScoresSpider, data=data_to_scrape)
    process.start()


if __name__ == '__main__':
    main()
