from os import environ
from os.path import abspath, join, dirname

BOOKMAKERS = [
    '188bet',
    '888sport',
    'betredkings',
    'bwin',
    'coral',
    'pinnaclesports',
    'sbobet',
    'sportingbet',
    'unibet',
    'williamhill',
]
SKIP_BETS = ['удары', 'пенальти', 'угловые', 'карт', 'проход', 'regular time', 'офсайд']
SPORTS = [
    'football'
]
START_BANK_VALUE = 1000

SITE_DIR = abspath(join(dirname(__file__), 'site'))
CLEAN_SCRAPED_AFTER_DAYS = 3

DEBUG = environ.get('DEBUG', False)

MONGODB_DATABASE = 'deepfork'
